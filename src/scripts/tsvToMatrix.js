export default function tsvToMatrix(tsvInText) {
    var rows = tsvInText.split('\n');
    for (var currentRowIndex = 0; currentRowIndex < rows.length; currentRowIndex++) {
        var cells = rows[currentRowIndex].split('\t').map(Number);
        rows[currentRowIndex] = cells;
    }
    return rows;
}