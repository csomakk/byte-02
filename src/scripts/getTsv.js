function getTsv(path, callback) {
    fetch(path)
    .then(function(response) {
        response.text().then(function (text) {
            callback(text.slice());
        });
    }).catch(function(error) {
        console.log("error:", error);
    });
}

module.exports.getTsv = getTsv;