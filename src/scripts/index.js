let checkSumTable = require("./checksumTable").default;
let tsvToMatrix = require("./tsvToMatrix").default;
let tsvGetter = require("./getTsv");

const path = '/resources/02-general.tsv';

tsvGetter.getTsv(path, function(result) {
    console.log(checkSumTable(tsvToMatrix(result)));
});