let checkSumRow = require("./checksumRow").default;

export default function getTableChecksum(matrix) {
    let result = 0;
    for (let i = 0; i < matrix.length; i++) {
        result += checkSumRow(matrix[i]);
    }
    return result;
}