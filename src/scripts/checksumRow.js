export default function getRowChecksum(rowArray) {
    
    if (!Array.isArray(rowArray)) {
        throw new Error("Row is not array");
    }
    if (rowArray.length === 0) {
        return 0;
    }
    
    let min = rowArray[0];
    let max = rowArray[0];

    for (let i = 0; i < rowArray.length; i++) {
        const currentItem = rowArray[i];
        if (isNaN(currentItem)) {
            throw new Error("Checksum has not a number");
        }
        if (currentItem < min) {
            min = currentItem;
        }
        if (currentItem > max) {
            max = currentItem;
        }
    }
    return max - min;
}