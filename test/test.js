var assert = require('assert');

import checksumTable from '../src/scripts/checksumTable';
import getRowChecksum from '../src/scripts/checksumRow';
import tsvToMatrix from '../src/scripts/tsvToMatrix';

describe("byte-homework-02-gabor-test--checksum", function() {
    describe("checksums", function() {
        it("gets row's partial checksum", function() {
            assert.equal(getRowChecksum([5, 1, 9, 5]), 8); 
            assert.equal(getRowChecksum([7, 5, 3]), 4);  
            assert.equal(getRowChecksum([2, 4, 6, 8]), 6);
            assert.equal(getRowChecksum([0, 1, 4]), 4);  
            assert.equal(getRowChecksum([]), 0);
            assert.equal(getRowChecksum([5]), 0);  
        });
        it("gets table checksum", function() {
            assert.equal(checksumTable([
                [5, 1, 9, 5],
                [7, 5, 3],
                [2, 4, 6, 8]
            ]), 18); 
        });
        it("throws error", function() {
            function testWithNanError() {
                getRowChecksum(["a"]);
            }
            assert.throws(testWithNanError, Error, "Checksum has not a number");

            function testWithNotArrayError() {
                getRowChecksum(3);
            }
            assert.throws(testWithNotArrayError, Error, "Row is not array");
        });    
    });
    describe("tsv", function() {
        it("converts tsv", function(){
            assert.deepEqual(tsvToMatrix("1 \t 4 \n    5 \t 8 \n   2 \t 5"), [[1,4], [5,8], [2,5]]);
        })
    });
    describe("integration", function() {
        it("integrates well", function() {
            assert.equal(checksumTable(tsvToMatrix("1 \t 4 \n    5 \t 8 \n   2 \t 5")), 9);
        });
    });
});