# Gabor Byte Homework 

built based on https://github.com/wbkd/webpack-starter

### Installation

```
npm install
npm install -g mocha
```

### Start Dev Server

```
npm start
```

### Build Prod Version

```
npm run build
```

### Test

```
npm test
```

### description
02-general.md has the problem description.

To load tsv, run the dev server.

### comment
Not a fiddle, as I figured loading an actual tsv file is more challenging. 